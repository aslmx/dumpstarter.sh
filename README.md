# dumpstarter.sh

A tiny yad powered gui to start tcpdumps:

![dumpstarter.sh screenshot](dumpstarter.png)

## Getting Started

dumpstarter.sh helps you setup a tcpdump in order to pipe the dump into wireshark. Everything that it does can be done without it, but it makes it a bit easier and lets you choose from predefined filters or connect to different capture hosts easily.

### Prerequisites

There are a few dependencies

* wireshark 
* yad
* awk, sed
* Optional: if you want to use adb, of course there must be adb in $PATH


```
sudo apt-get install yad
```

### Install and run

Install dependencies (see above), then

```

git clone https://gitlab.com/aslmx/dumpstarter.sh.git
cd dumpstarter.sh
cp filter.txt ~/.dumpfilter.txt

```

Optional: install symlink

```
sudo ln -s /home/user/tools/dumpstarter/dumpstarter.sh /usr/sbin/dumpstarter
```

Optional: copy desktop file into applications
(Might depend on your distribution)

```
sudo cp dumpstart.desktop /usr/share/applications/
```

Mint has wireshark icons here, if you need one:
```
/usr/share/icons/hicolor/128x128/apps/wireshark.png
```

Simply run dumpstarter.sh - either via commandline or via start menu (if you copied the .desktop file

dumpstarter.sh has no commandline arguments.

## Limitations ##

My coding skills are limited. I tested a lot, but only on my computer. 
As dependencies are only very basic, I don't expect things to not work, but you should not expect ;-)
Feel free to let me know of any issues. 

### Interfaces ###

The interfaces list is partially hardcoded in the script file.

We can not read remote servers interfaces easily (at least it is beyond reasonable for now)

You can easily add any interface name you are using on a remote machine by finding and changing the following line

```
interfaces=$(echo "eth1,eth0,any")              ## hardcoded interface, change here if you need to adapt!
```

At the start of the script, local interfaces (interfaces on **YOUR local machine**) are read out using ifconfig and then added to the list of static interfaces.


### Parsing ###

There is no checking on parsing the .dumpfilters.txt file. Run on your own risk.

### local tcpdump mode ###

Make sure tcpdump is started with correct permissions. I.e. sudo in the most environments!

Either start dumpstarter.sh as sudo, or if you do not wish to do so, you can run an arbitrary command as sudo and directly run dumpstarter afterwards.
Usually sudo is configured to ask you only every few minutes for your password. 

```
sudo uptime
./dumpstarter.sh
```



## .dumpfilters.txt ##

Put the filters.txt file to ~/.dumpfilters.txt (optionally, you can change the hardcoded path in the script file if you like).

The syntax is:

```
filtername1
filterstring1
filtername2
filterstring2
filtername3
filterstring3
filtername_n
filterstring_n
```

Here is a list of useful links for the pcap capture filter format:

* http://packetlife.net/media/library/12/tcpdump.pdf
* https://www.andreafortuna.org/2018/07/18/tcpdump-a-simple-cheatsheet/
* https://hackertarget.com/tcpdump-examples/ 


## Misc

Feel free to comment / raise issues / praise ....

## Built With

* GNU nano
* love
* passion

## Authors

* aslmx@gmx.de

## License

This project is given to public domain.

## Acknowledgments

* Hat tip to anyone whose code was used
* https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
* https://blog.wirelessmoves.com/2017/02/adb-and-tcpdump-on-android-for-live-wireshark-tracing.html


