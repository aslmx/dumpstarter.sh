#!/bin/bash


### dumpstart.sh
### (c) 2019 aslmx@gmx.de
###
### helps setting up a tcpdump session and pipe it into wireshark
### dependices: (bash, ssh, adb,  yad) 
###
###

function install_yad()
{
read -p "I will apt-get install yad for you. Do you want to continue? Type Y or y to continue or any other key to abort: " -n 1 -r
echo ""   # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo apt-get install yad
else
	exit 1
fi
}


## check dependencies
command -v yad &> /dev/null || { echo >&2 "I need yad installed."; install_yad || exit 1; }
command -v adb &> /dev/null || { echo >&2 "ADB is recommended."; useadb="no";  }


T_Timestamp=`date "+%Y-%m-%d_%H-%M_"`
echo $T_Timestamp
servers=$(echo "pi@192.168.2.100,root@192.168.2.80")
interfaces=$(echo "eth1,eth0,any")
filters=$(echo "not port 22,EXTENDED,not port 22 and host 8.8.8.8")

# Read Adb devices
# do not pipe adb devices into while loop, as of
# https://unix.stackexchange.com/questions/402750/modify-global-variable-in-while-loop
# check if useadb was set first (if it was set then probably to "no", if true skip reading device list
if [ -z "$useadb" ]; then
	while read line; do
	echo "adding $line"

	if [ -z "$T_devices" ]; then
		T_devices="$line"
	else
		T_devices="$T_devices,$line"
	fi

	done <<<$(adb devices -l | grep -i -v list)

echo $T_devices

fi


## Function to show extended Filters list 
function extended_filters()
{


filter=`yad --width=600 --height=400  --title="Choose extended filter" --text="\n Filters are read from $HOME/.dumpfilters.txt\n" \
	--image="/usr/share/icons/hicolor/128x128/apps/wireshark.png" \
	--list --column="Name"  --column="Filter" < $HOME/.dumpfilters.txt`

if [ $? -eq 0 ]; then
        T_capturefilter=`echo $filter | awk -F'|' '{print $2}'`
        echo "capture filter: $T_capturefilter"
else
        echo "aborted"
        exit 1
fi

}

function generate_interfaces()
{

while read line; do
        echo "adding interface $line"

        if [ -z "$interfaces" ]; then
                interfaces="$line"
        else
                interfaces="$interfaces,$line"
        fi

        done <<<$(ifconfig -a | sed 's/[ \t].*//;/^\(lo\|\)$/d' | awk -F':' '{print $1}')

}


generate_interfaces



####
####
####
####### Main YAD Screen call
####
####
####

tracing=`yad --width=1000 --title="Dumpstarter" --text "\nPlease choose Tracing options:\n"\
	--image="/usr/share/icons/hicolor/128x128/apps/wireshark.png" \
	--form --separator="," --item-separator=","  \
	--field="Tracing Target":CB \
	--field="Tracing Server":CBE \
	--field="Device":CBE  \
	--field="Interface":CBE \
	--field="Autosave::CB"  \
	--field="Location:":MDIR \
	--field="Filename:" \
	--field="Filter":CBE --width=400\
	"SSH,ADB,LOCAL" "$servers" "$T_devices" "$interfaces" "YES,NO" "$HOME" "$T_Timestamp" "$filters"`

### Check if OKAY was pressed 
if [ $? -eq 0 ]; then

	# Process YAD output and put it into the correct variables

	T_target=`echo $tracing | awk -F',' '{print $1}'`
	T_host=`echo $tracing | awk -F',' '{print $2}'`
	T_device=`echo $tracing | awk -F',' '{print $3}'`
	T_interface=`echo $tracing | awk -F',' '{print $4}'`
	T_autosave=`echo $tracing | awk -F',' '{print $5}'`
	T_dir=`echo $tracing | awk -F',' '{print $6}'`
	T_file=`echo $tracing | awk -F',' '{print $7}'`
	T_capturefilter=`echo $tracing | awk -F',' '{print $8}'`

	## Check if Extended filters have been chosen. If yes start extended dialog
	if [ "$T_capturefilter" = "EXTENDED" ]; then
		echo "show extended filters...."

		extended_filters

		echo "Extended filter: $T_capturefilter"

	else
		echo load normal ....
	fi


	# Check if AUtosave is chosen, if yes prepare  option to put into wireshark call
	if [ "$T_autosave" = "YES" ]; then
		T_filename="$T_dir/$T_file"
		T_saveto="-w $T_filename"
		echo "auto saving to: $T_saveto"
	else
		T_saveto=""
	fi
		### T_saveto is added to wireshark call later. If it is empty it does not have any impact


	### Check Tracing Target
	if [ "$T_target" = "SSH" ]
	then

		# prepare SSH Calll
		echo "SSH"

		#T_command="ssh $T_host "
		T_command="ssh $T_host \"sudo tcpdump -i $T_interface -U -w - '$T_capturefilter'\""

	elif [ "$T_target" = "ADB" ]
	then
		# prepare ADB exec
		echo "ADB"
		## example Call:
		#alias acap='adb exec-out '\''tcpdump -i any -U -w - 2>/dev/null'\'' | wireshark -k -S -i -'

		# Post process device... just take first part (Serial Number from adb devices -l)
		# device is targeted with "adb -s {serial}"
		T_device=`echo $T_device | awk -F' ' '{print $1}'`

		T_command="adb -s $T_device exec-out \"tcpdump -i $T_interface -U -w - '$T_capturefilter' 2>/dev/null \""




	elif [ "$T_target" = "LOCAL" ]
	then

		T_command="sudo tcpdump -i $T_interface -U -w - '$T_capturefilter'"

	else
		echo "Nope..." 
		exit 1
	fi




	echo "YAD output: $tracing"
	echo -e "\n\n Target= $T_target \n \n Hostname: $T_host \n DEVICE: $T_device \n Interface: $T_interface \n Autosave: $T_autosave ->  Saveto: $T_saveto \n Capture filter:  $T_capturefilter \n\n"
	echo -e "\n\n Actual tracing command: $T_command  \n\n" 

	# eval starts a string as a command

	eval $T_command | wireshark -S -k $T_saveto -i -

else
	echo "Abort..."
fi
